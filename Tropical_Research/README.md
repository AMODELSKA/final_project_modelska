## EW Project / 573 Project

The files that are in this repository are as follows:
-an environment.yml

-my project for this class & simultaneously it's also the work I have done for the tropical research group it's called: EWProject_Adrianna.ipynb ( this is the notebook I want graded!)

-note: there is another notebook called EWProject_pt2. I have another task to do for research and didn't want to overload/confuse it with my project for this class. I will be working on this over break but it should be blank at the moment but if I end up working on it during the next few days, it might not be blank when you look at it. This isn't the notebook I want graded for the 573 final project!

In addition I have both of the datasets uploaded in this folder (the clausir...nc and the era5_global..nc). Both of these NetCDF files were given to me through my research group. I made a copy of it through either Victor's or Rosa's folders in adhara and downloaded and uploaded them into this jupyter directory. 